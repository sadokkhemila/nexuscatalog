package metier;

import java.util.ArrayList;
import java.util.List;

public interface IcatalogueMetier  {
	public void addproduit(produit p);
	public List<produit> listprod ();
	public List<produit> listprodmc (String mc);
	public produit getproduit(String ref);
	public void updateproduit(produit p);
	public void deleteproduit(String ref);

}
