package web;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import metier.IcatalogueMetier;
import metier.catalogueimpl;
import metier.produit;

public class controleurservlet extends HttpServlet {
	private IcatalogueMetier metier;
	
	@Override
	public void init() throws ServletException {
		metier=new catalogueimpl();
	}
@Override
protected void doGet(HttpServletRequest request, HttpServletResponse response)
		throws ServletException, IOException {
	//appel de methode do post pour que on peut utiliser soit get ou post
	doPost(request, response);
}
@Override
protected void doPost(HttpServletRequest request, HttpServletResponse response) 
		throws ServletException, IOException {
	//instancier ou crer le model
	modelproduit model=new modelproduit();
	
	//definir variable action qui contient valeur de l'action retourner par request
		String action=request.getParameter("action");
		if(action!=null) {
			if(action.equals("chercher")) {
				//saisir le mot cle et le stoquer dans le model
				model.setMotcle(request.getParameter("motcle"));
				//appel al couche metier et recupere le resultat
				List<produit> produits=metier.listprodmc(model.getMotcle());
				//stocker le resultat dans le model
				model.setProduits(produits);
	
			}
			
			else if(action.equals("delete")) {
				String ref=request.getParameter("ref");
				metier.deleteproduit(ref);
				//stocker le resultat dans model
				model.setProduits(metier.listprod());
						
				}
			
			else if(action.equals("edit")) {
				String ref=request.getParameter("ref");
				produit p=metier.getproduit(ref);
				model.setProduit(p);
				model.setMode("edit");
				model.setProduits(metier.listprod());;
			}
			
			else if(action.equals("ajouter")) {
				model.getProduit().setReference(request.getParameter("reference"));
				model.getProduit().setDesignation(request.getParameter("designation"));
				model.getProduit().setPrix(Double.parseDouble(request.getParameter("prix")));
				model.getProduit().setQuantite(Integer.parseInt(request.getParameter("quantite")));
				model.setMode(request.getParameter("mode"));
				if(model.getMode().equals("ajout")) 
				metier.addproduit(model.getProduit());
				else if(model.getMode().equals("edit")) 
				metier.updateproduit(model.getProduit());
				model.setProduits(metier.listprod());
				
			}
		}
		//stocker le model dans request
	request.setAttribute("model", model);
	request.getRequestDispatcher("vueproduit.jsp").forward(request, response);
}
}
