package web;

import java.util.ArrayList;
import java.util.List;



import metier.produit;

public class modelproduit {
 private String motcle;
 private produit produit=new produit();
 private String mode="ajout";
 private List<produit> produits=new ArrayList<>();
public String getMotcle() {
	return motcle;
}
public void setMotcle(String motcle) {
	this.motcle = motcle;
}
public List<produit> getProduits() {
	return produits;
}
public void setProduits(List<produit> produits) {
	this.produits = produits;
}
public produit getProduit() {
	return produit;
}
public void setProduit(produit produit) {
	this.produit = produit;
}
public String getMode() {
	return mode;
}
public void setMode(String mode) {
	this.mode = mode;
}

}
